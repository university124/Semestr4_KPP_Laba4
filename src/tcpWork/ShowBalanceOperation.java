package tcpWork;

import java.io.Serial;

public class ShowBalanceOperation extends CardOperation {
    @Serial
    private static final long serialVersionUID = 1L;
    private String serNum = null;

    public ShowBalanceOperation(String serNum) {
        this.serNum = serNum;
    }

    public ShowBalanceOperation() {}

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }
}
