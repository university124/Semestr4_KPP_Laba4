package tcpWork;

import java.io.Serial;

public class AddMetroCardOperation extends CardOperation {
    @Serial
    private static final long serialVersionUID = 1L;
    private MetroCard crd = null;

    public AddMetroCardOperation() {
        crd = new MetroCard();
    }

    public MetroCard getCrd() {
        return crd;
    }
}
