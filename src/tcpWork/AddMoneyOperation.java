package tcpWork;

import java.io.Serial;

public class AddMoneyOperation extends CardOperation {
    @Serial
    private static final long serialVersionUID = 1L;
    private String serNum = null;
    private double money = 0.0;

    public AddMoneyOperation(String serNum, double money) {
        this.serNum = serNum;
        this.money = money;
    }

    public AddMoneyOperation() {
        this("null", 0.0);
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }
}
