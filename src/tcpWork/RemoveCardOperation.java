package tcpWork;

import java.io.Serial;

public class RemoveCardOperation extends CardOperation {
    @Serial
    private static final long serialVersionUID = 1L;
    private String serNum = null;

    public RemoveCardOperation(String serNum) {
        this.serNum = serNum;
    }

    public RemoveCardOperation() {}

    public String getSerNum() {
        return serNum;
    }

    public void setSerNum(String serNum) {
        this.serNum = serNum;
    }
}
