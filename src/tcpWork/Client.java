package tcpWork;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Client {
    private int port = -1;
    private String server = null;
    private Socket socket = null;
    private ObjectInputStream is = null;
    private ObjectOutputStream os = null;

    public Client(String server, int port) {
        this.port = port;
        this.server = server;
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(server, port), 1000);
            os = new ObjectOutputStream(socket.getOutputStream());
            is = new ObjectInputStream(socket.getInputStream());
        } catch (InterruptedIOException e) {
            System.out.println("Error: " + e);
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
    }

    public void finish() {
        try {
            os.writeObject(new StopOperation());
            os.flush();
            System.out.println(is.readObject());
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Error: " + ex);
        }
    }

    public void applyOperation(CardOperation op) {
        try {
            os.writeObject(op);
            os.flush();
            System.out.println(is.readObject());
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("Error: " + ex);
        }
    }

    public static void main(String[] args) {    // TODO Запуск только после изменения на ручную сериализацию во всех классах
        // Создание и регистрация новой карты
        Client cl = new Client("localhost", 7891);
        AddMetroCardOperation op = new AddMetroCardOperation();
        op.getCrd().setUsr(new User("Stepan", "Stepanov", "M", "23.04.2004"));
        op.getCrd().setSerNum("00002");
        op.getCrd().setColledge("Karazina");
        op.getCrd().setBalance(50);
        cl.applyOperation(op);
        cl.finish();

        // Пополнение баланса
        cl = new Client("localhost", 7891);
        cl.applyOperation(new AddMoneyOperation("00002", 200));
        cl.finish();

        // Запрос баланса
        cl = new Client("localhost", 7891);
        cl.applyOperation(new ShowBalanceOperation("00002"));
        cl.finish();

        // Оплата поездки
        cl = new Client("localhost", 7891);
        cl.applyOperation(new PayMoneyOperation("00002", 30));
        cl.finish();

        // Запрос баланса после оплаты поездки
        cl = new Client("localhost", 7891);
        cl.applyOperation(new ShowBalanceOperation("00002"));
        cl.finish();

        // Удаление карты
        cl = new Client("localhost", 7891);
        cl.applyOperation(new RemoveCardOperation("00002"));
        cl.finish();

        // Попытка запросить баланс удаленной карты (должна завершиться неудачно)
        cl = new Client("localhost", 7891);
        cl.applyOperation(new ShowBalanceOperation("00002"));
        cl.finish();

        // Создание другой карты
        cl = new Client("localhost", 7891);
        op = new AddMetroCardOperation();
        op.getCrd().setUsr(new User("Semen", "Semenov", "M", "07.05.2002"));
        op.getCrd().setSerNum("00003");
        op.getCrd().setColledge("HNURE"); // Не уверен, что ХНУРЭ пишется так латиницей
        op.getCrd().setBalance(100);
        cl.applyOperation(op);
        cl.finish();

        // Пополнение баланса отрицательной суммой (должно завершиться неудачно)
        cl = new Client("localhost", 7891);
        cl.applyOperation(new AddMoneyOperation("00003", -50));
        cl.finish();

        // Попытка оплаты поездки с недостаточным балансом
        cl = new Client("localhost", 7891);
        cl.applyOperation(new PayMoneyOperation("00003", 150));
        cl.finish();

        // Удаление несуществующей карты
        cl = new Client("localhost", 7891);
        cl.applyOperation(new RemoveCardOperation("99999"));
        cl.finish();

        // Запрос баланса существующей карты
        cl = new Client("localhost", 7891);
        cl.applyOperation(new ShowBalanceOperation("00003"));
        cl.finish();
    }
}
